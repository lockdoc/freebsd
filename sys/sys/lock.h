/*-
 * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
 *
 * Copyright (c) 1997 Berkeley Software Design, Inc. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Berkeley Software Design Inc's name may not be used to endorse or
 *    promote products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY BERKELEY SOFTWARE DESIGN INC ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL BERKELEY SOFTWARE DESIGN INC BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 *	from BSDI Id: mutex.h,v 2.7.2.35 2000/04/27 03:10:26 cp
 * $FreeBSD$
 */

#ifndef _SYS_LOCK_H_
#define _SYS_LOCK_H_

#include <sys/queue.h>
#include <sys/_lock.h>
#include <sys/ktr_class.h>
#include <sys/lock_class.h>
#include <sys/lockdoc.h>

struct lock_list_entry;
struct thread;


#define	LC_SLEEPLOCK	0x00000001	/* Sleep lock. */
#define	LC_SPINLOCK	0x00000002	/* Spin lock. */
#define	LC_SLEEPABLE	0x00000004	/* Sleeping allowed with this lock. */
#define	LC_RECURSABLE	0x00000008	/* Locks of this type may recurse. */
#define	LC_UPGRADABLE	0x00000010	/* Upgrades and downgrades permitted. */

/*
 * Option flags passed to lock operations that witness also needs to know
 * about or that are generic across all locks.
 */
#define	LOP_NEWORDER	0x00000001	/* Define a new lock order. */
#define	LOP_QUIET	0x00000002	/* Don't log locking operations. */
#define	LOP_TRYLOCK	0x00000004	/* Don't check lock order. */
#define	LOP_EXCLUSIVE	0x00000008	/* Exclusive lock. */
#define	LOP_DUPOK	0x00000010	/* Don't check for duplicate acquires */
#define	LOP_NOSLEEP	0x00000020	/* Non-sleepable despite LO_SLEEPABLE */

/* Flags passed to witness_assert. */
#define	LA_MASKASSERT	0x000000ff	/* Mask for witness defined asserts. */
#define	LA_UNLOCKED	0x00000000	/* Lock is unlocked. */
#define	LA_LOCKED	0x00000001	/* Lock is at least share locked. */
#define	LA_SLOCKED	0x00000002	/* Lock is exactly share locked. */
#define	LA_XLOCKED	0x00000004	/* Lock is exclusively locked. */
#define	LA_RECURSED	0x00000008	/* Lock is recursed. */
#define	LA_NOTRECURSED	0x00000010	/* Lock is not recursed. */

#ifdef _KERNEL
/*
 * Macros for KTR_LOCK tracing.
 *
 * opname  - name of this operation (LOCK/UNLOCK/SLOCK, etc.)
 * lo      - struct lock_object * for this lock
 * flags   - flags passed to the lock operation
 * recurse - this locks recursion level (or 0 if class is not recursable)
 * result  - result of a try lock operation
 * file    - file name
 * line    - line number
 */
#if LOCK_DEBUG > 0
#define	LOCK_LOG_TEST(lo, flags)					\
	(((flags) & LOP_QUIET) == 0 && ((lo)->lo_flags & LO_QUIET) == 0)
#else
#define	LOCK_LOG_TEST(lo, flags)	0
#endif

#define	LOCK_LOG_LOCK(opname, lo, flags, recurse, file, line) do {	\
	if (LOCK_LOG_TEST((lo), (flags)))				\
		CTR6(KTR_LOCK, opname " (%s) %s %p r = %d at %s:%d",	\
		    LOCK_CLASS(lo)->lc_name, (lo)->lo_name,		\
		    (lo), (u_int)(recurse), (file), (line));		\
} while (0)

#define	LOCK_LOG_TRY(opname, lo, flags, result, file, line) do {	\
	if (LOCK_LOG_TEST((lo), (flags)))				\
		CTR6(KTR_LOCK, "TRY_" opname " (%s) %s %p result=%d at %s:%d",\
		    LOCK_CLASS(lo)->lc_name, (lo)->lo_name,		\
		    (lo), (u_int)(result), (file), (line));		\
} while (0)

#define	LOCK_LOG_INIT(lo, flags) do {					\
	if (LOCK_LOG_TEST((lo), (flags)))				\
		CTR4(KTR_LOCK, "%s: %p (%s) %s", __func__, (lo),	\
 		    LOCK_CLASS(lo)->lc_name, (lo)->lo_name);		\
} while (0)

#define	LOCK_LOG_DESTROY(lo, flags)	LOCK_LOG_INIT(lo, flags)

#define	lock_initialized(lo)	((lo)->lo_flags & LO_INITIALIZED)

extern struct lock_class lock_class_mtx_sleep;
extern struct lock_class lock_class_mtx_spin;
extern struct lock_class lock_class_sx;
extern struct lock_class lock_class_rw;
extern struct lock_class lock_class_rm;
extern struct lock_class lock_class_rm_sleepable;
extern struct lock_class lock_class_lockmgr;

struct lock_delay_config {
	u_short base;
	u_short max;
};

extern struct lock_delay_config locks_delay;
extern u_short locks_delay_retries;
extern u_short locks_delay_loops;

struct lock_delay_arg {
	struct lock_delay_config *config;
	u_short delay;
	u_int spin_cnt;
};

static inline void
lock_delay_arg_init(struct lock_delay_arg *la, struct lock_delay_config *lc)
{
	la->config = lc;
	la->delay = lc->base;
	la->spin_cnt = 0;
}

static inline void
lock_delay_arg_init_noadapt(struct lock_delay_arg *la)
{
	la->delay = 0;
	la->spin_cnt = 0;
}

#define lock_delay_spin(n)	do {	\
	u_int _i;			\
					\
	for (_i = (n); _i > 0; _i--)	\
		cpu_spinwait();		\
} while (0)

#define	LOCK_DELAY_SYSINIT(func) \
	SYSINIT(func##_ld, SI_SUB_LOCK, SI_ORDER_ANY, func, NULL)

#define	LOCK_DELAY_SYSINIT_DEFAULT(lc) \
	SYSINIT(lock_delay_##lc##_ld, SI_SUB_LOCK, SI_ORDER_ANY, \
	    lock_delay_default_init, &lc)

void	lock_init(struct lock_object *, struct lock_class *,
	    const char *, const char *, int);
void	lock_destroy(struct lock_object *);
void	lock_delay(struct lock_delay_arg *);
void	lock_delay_default_init(struct lock_delay_config *);
void	spinlock_enter(void);
void	spinlock_exit(void);
void	witness_init(struct lock_object *, const char *);
void	witness_destroy(struct lock_object *);
int	witness_defineorder(struct lock_object *, struct lock_object *);
void	witness_checkorder(struct lock_object *, int, const char *, int,
	    struct lock_object *);
void	witness_lock(struct lock_object *, int, const char *, int);
void	witness_upgrade(struct lock_object *, int, const char *, int);
void	witness_downgrade(struct lock_object *, int, const char *, int);
void	witness_unlock(struct lock_object *, int, const char *, int);
void	witness_save(struct lock_object *, const char **, int *);
void	witness_restore(struct lock_object *, const char *, int);
int	witness_list_locks(struct lock_list_entry **,
	    int (*)(const char *, ...));
int	witness_warn(int, struct lock_object *, const char *, ...);
void	witness_assert(const struct lock_object *, int, const char *, int);
void	witness_display_spinlock(struct lock_object *, struct thread *,
	    int (*)(const char *, ...));
int	witness_line(struct lock_object *);
void	witness_norelease(struct lock_object *);
void	witness_releaseok(struct lock_object *);
const char *witness_file(struct lock_object *);
void	witness_thread_exit(struct thread *);

#ifdef	WITNESS
int	witness_startup_count(void);
void	witness_startup(void *);

/* Flags for witness_warn(). */
#define	WARN_GIANTOK	0x01	/* Giant is exempt from this check. */
#define	WARN_PANIC	0x02	/* Panic if check fails. */
#define	WARN_SLEEPOK	0x04	/* Sleepable locks are exempt from check. */

#define	WITNESS_INIT(lock, type)					\
	witness_init((lock), (type))

#define WITNESS_DESTROY(lock)						\
	witness_destroy(lock)

#define	WITNESS_CHECKORDER(lock, flags, file, line, interlock)		\
	witness_checkorder((lock), (flags), (file), (line), (interlock))

#define	WITNESS_DEFINEORDER(lock1, lock2)				\
	witness_defineorder((struct lock_object *)(lock1),		\
	    (struct lock_object *)(lock2))

#define	WITNESS_LOCK(lock, flags, file, line) do {			\
	witness_lock((lock), (flags), (file), (line));			\
	if ((flags) & LOP_EXCLUSIVE) {					\
	log_lock(P_WRITE, (lock), (file), (line), LOCK_NONE);		\
	} else {							\
	log_lock(P_READ, (lock), (file), (line), LOCK_NONE);		\
	}								\
	} while (0);

#define	WITNESS_UPGRADE(lock, flags, file, line) do {			\
	witness_upgrade((lock), (flags), (file), (line));		\
	log_lock(V_READ, (lock), (file), (line), LOCK_NONE);		\
	log_lock(P_WRITE, (lock), (file), (line), LOCK_NONE);		\
	} while (0);

#define	WITNESS_DOWNGRADE(lock, flags, file, line) do {			\
	witness_downgrade((lock), (flags), (file), (line));		\
	log_lock(V_WRITE, (lock), (file), (line), LOCK_NONE);		\
	log_lock(P_READ, (lock), (file), (line), LOCK_NONE);		\
	} while (0);			\

#define	WITNESS_UNLOCK(lock, flags, file, line) do {			\
	witness_unlock((lock), (flags), (file), (line));			\
	if ((flags) & LOP_EXCLUSIVE) {					\
	log_lock(V_WRITE, (lock), (file), (line), LOCK_NONE);		\
	} else {							\
	log_lock(V_READ, (lock), (file), (line), LOCK_NONE);		\
	}								\
	} while (0);

#define	WITNESS_UNLOCK_NOLOG(lock, flags, file, line) do {			\
	witness_unlock((lock), (flags), (file), (line));

#define	WITNESS_CHECK(flags, lock, fmt, ...)				\
	witness_warn((flags), (lock), (fmt), ## __VA_ARGS__)

#define	WITNESS_WARN(flags, lock, fmt, ...)				\
	witness_warn((flags), (lock), (fmt), ## __VA_ARGS__)

#define	WITNESS_SAVE_DECL(n)						\
	const char * __CONCAT(n, __wf);					\
	int __CONCAT(n, __wl)

#define	WITNESS_SAVE(lock, n) 						\
	witness_save((lock), &__CONCAT(n, __wf), &__CONCAT(n, __wl))

#define	WITNESS_RESTORE(lock, n) 					\
	witness_restore((lock), __CONCAT(n, __wf), __CONCAT(n, __wl))

#define	WITNESS_NORELEASE(lock)						\
	witness_norelease(&(lock)->lock_object)

#define	WITNESS_RELEASEOK(lock)						\
	witness_releaseok(&(lock)->lock_object)

#define	WITNESS_FILE(lock) 						\
	witness_file(lock)

#define	WITNESS_LINE(lock) 						\
	witness_line(lock)

#elif defined(LOCKDOC_RECORD)	/* !WITNESS && LOCKDOC_RECORD */
#define	WITNESS_INIT(lock, type)				(void)0
#define	WITNESS_DESTROY(lock)					(void)0
#define	WITNESS_DEFINEORDER(lock1, lock2)	0
#define	WITNESS_CHECKORDER(lock, flags, file, line, interlock)	(void)0
#define	WITNESS_LOCK(lock, flags, file, line) do {		\
	if ((flags) & LOP_EXCLUSIVE) {				\
	log_lock(P_WRITE, (lock), (file), (line), LOCK_NONE);	\
	} else {						\
	log_lock(P_READ, (lock), (file), (line), LOCK_NONE);	\
	}							\
	} while (0);
#define	WITNESS_UPGRADE(lock, flags, file, line) do {			\
	log_lock(V_READ, (lock), (file), (line), LOCK_NONE);		\
	log_lock(P_WRITE, (lock), (file), (line), LOCK_NONE);		\
	} while (0);
#define	WITNESS_DOWNGRADE(lock, flags, file, line) do {			\
	log_lock(V_WRITE, (lock), (file), (line), LOCK_NONE);		\
	log_lock(P_READ, (lock), (file), (line), LOCK_NONE);		\
	} while (0);
#define	WITNESS_UNLOCK(lock, flags, file, line) do {		\
	if ((flags) & LOP_EXCLUSIVE) {				\
	log_lock(V_WRITE, (lock), (file), (line), LOCK_NONE);	\
	} else {						\
	log_lock(V_READ, (lock), (file), (line), LOCK_NONE);	\
	}							\
	} while (0);
#define	WITNESS_UNLOCK_NOLOG(lock, flags, file, line)		(void)0
#define	WITNESS_CHECK(flags, lock, fmt, ...)	0
#define	WITNESS_WARN(flags, lock, fmt, ...)			(void)0
#define	WITNESS_SAVE_DECL(n)					(void)0
#define	WITNESS_SAVE(lock, n)					(void)0
#define	WITNESS_RESTORE(lock, n)				(void)0
#define	WITNESS_NORELEASE(lock)					(void)0
#define	WITNESS_RELEASEOK(lock)					(void)0
#define	WITNESS_FILE(lock) ("?")
#define	WITNESS_LINE(lock) (0)
#else /* !WITNESS && !LOCKDOC_RECORD */
#define	WITNESS_INIT(lock, type)				(void)0
#define	WITNESS_DESTROY(lock)					(void)0
#define	WITNESS_DEFINEORDER(lock1, lock2)	0
#define	WITNESS_CHECKORDER(lock, flags, file, line, interlock)	(void)0
#define	WITNESS_LOCK(lock, flags, file, line)			(void)0
#define	WITNESS_UPGRADE(lock, flags, file, line)		(void)0
#define	WITNESS_DOWNGRADE(lock, flags, file, line)		(void)0
#define	WITNESS_UNLOCK(lock, flags, file, line)			(void)0
#define	WITNESS_UNLOCK_NOLOG(lock, flags, file, line)		(void)0
#define	WITNESS_CHECK(flags, lock, fmt, ...)	0
#define	WITNESS_WARN(flags, lock, fmt, ...)			(void)0
#define	WITNESS_SAVE_DECL(n)					(void)0
#define	WITNESS_SAVE(lock, n)					(void)0
#define	WITNESS_RESTORE(lock, n)				(void)0
#define	WITNESS_NORELEASE(lock)					(void)0
#define	WITNESS_RELEASEOK(lock)					(void)0
#define	WITNESS_FILE(lock) ("?")
#define	WITNESS_LINE(lock) (0)
#endif	/* WITNESS */

#endif	/* _KERNEL */
#endif	/* _SYS_LOCK_H_ */
