#ifndef _SYS_LOCK_CLASS_H_
#define _SYS_LOCK_CLASS_H_

#define	LO_CLASSFLAGS	0x0000ffff	/* Class specific flags. */
#define	LO_INITIALIZED	0x00010000	/* Lock has been initialized. */
#define	LO_WITNESS	0x00020000	/* Should witness monitor this lock. */
#define	LO_QUIET	0x00040000	/* Don't log locking operations. */
#define	LO_RECURSABLE	0x00080000	/* Lock may recurse. */
#define	LO_SLEEPABLE	0x00100000	/* Lock may be held while sleeping. */
#define	LO_UPGRADABLE	0x00200000	/* Lock may be upgraded/downgraded. */
#define	LO_DUPOK	0x00400000	/* Don't check for duplicate acquires */
#define	LO_IS_VNODE	0x00800000	/* Tell WITNESS about a VNODE lock */
#define	LO_CLASSMASK	0x0f000000	/* Class index bitmask. */
#define LO_NOPROFILE    0x10000000      /* Don't profile this lock */
#define	LO_NEW		0x20000000	/* Don't check for double-init */

/*
 * Lock classes are statically assigned an index into the gobal lock_classes
 * array.  Debugging code looks up the lock class for a given lock object
 * by indexing the array.
 */
#define	LO_CLASSSHIFT		24
#define	LO_CLASSINDEX(lock)	((((lock)->lo_flags) & LO_CLASSMASK) >> LO_CLASSSHIFT)
#define	LOCK_CLASS(lock)	(lock_classes[LO_CLASSINDEX((lock))])
#define	LOCK_CLASS_MAX		(LO_CLASSMASK >> LO_CLASSSHIFT)

struct thread;
struct lock_object;

/*
 * Lock classes.  Each lock has a class which describes characteristics
 * common to all types of locks of a given class.
 *
 * Spin locks in general must always protect against preemption, as it is
 * an error to perform any type of context switch while holding a spin lock.
 * Also, for an individual lock to be recursable, its class must allow
 * recursion and the lock itself must explicitly allow recursion.
 *
 * The 'lc_ddb_show' function pointer is used to dump class-specific
 * data for the 'show lock' DDB command.  The 'lc_lock' and
 * 'lc_unlock' function pointers are used in sleep(9) and cv_wait(9)
 * to lock and unlock locks while blocking on a sleep queue.  The
 * return value of 'lc_unlock' will be passed to 'lc_lock' on resume
 * to allow communication of state between the two routines.
 */

struct lock_class {
	const		char *lc_name;
	u_int		lc_flags;
	void		(*lc_assert)(const struct lock_object *lock, int what);
	void		(*lc_ddb_show)(const struct lock_object *lock);
	void		(*lc_lock)(struct lock_object *lock, uintptr_t how);
	int		(*lc_owner)(const struct lock_object *lock,
			    struct thread **owner);
	uintptr_t	(*lc_unlock)(struct lock_object *lock);
};

extern struct lock_class *lock_classes[];
#endif /* _SYS_LOCK_CLASS_H_ */
