#if !defined(__LOCKDOC_H__) && !defined(__ASSEMBLER__)
#define __LOCKDOC_H__

#include <sys/types.h>
#include <sys/lockdoc_event.h>
#include <sys/libkern.h>
#include <sys/lock_class.h>
#include <machine/frame.h>

#define DELIMITER	"#"
#define DELIMITER_CHAR	'#'
#define PING_CHAR	'p'
#define MK_STRING(x)	#x
#define IO_PORT_LOG	(0x00e9)

#ifdef LOCKDOC_RECORD

#define TRACE_IRQS_ON_CHECK(x) do {	\
	register_t cur_eflags;	\
	cur_eflags = read_eflags();	\
	if (((x) & (1 << 9)) && !(cur_eflags & (1 << 9))) {	\
		log_lock(V_WRITE, (struct lock_object*)PSEUDOLOCK_ADDR_HARDIRQ, __FILE__, __LINE__, 0);	\
	}	\
} while (0);
#define TRACE_IRQS_ON() do {	\
	log_lock(V_WRITE, (struct lock_object*)PSEUDOLOCK_ADDR_HARDIRQ, __FILE__, __LINE__, 0);	\
} while (0);

#define TRACE_IRQS_OFF_CHECK(x) do {	\
	register_t cur_eflags;	\
	cur_eflags = read_eflags();	\
	if (((x) & (1 << 9)) && !(cur_eflags & (1 << 9))) {	\
		log_lock(P_WRITE, (struct lock_object*)PSEUDOLOCK_ADDR_HARDIRQ, __FILE__, __LINE__, 0);	\
	}	\
} while (0);
#define TRACE_IRQS_OFF() do {	\
	log_lock(P_WRITE, (struct lock_object*)PSEUDOLOCK_ADDR_HARDIRQ, __FILE__, __LINE__, 0);	\
} while (0);

void trace_irqs_on(struct trapframe *frame);
void trace_irqs_off(struct trapframe *frame);
int32_t lockdoc_get_ctx(void);
extern struct log_action la_buffer;

/* Basic port I/O */
static inline void __outb(u_int8_t v, u_int16_t port)
{
	__asm __volatile("outb %0,%1" : : "a" (v), "dN" (port));
}

static inline void log_memory(int alloc, const char *datatype, const void *ptr, size_t size) {
	register_t flags;
	
	flags = lockdoc_intr_disable();

	memset(&la_buffer,0,sizeof(la_buffer));

	la_buffer.action = (alloc == 1 ? LOCKDOC_ALLOC : LOCKDOC_FREE);
	la_buffer.ptr = (unsigned long)ptr;
	la_buffer.size = size;
	la_buffer.ctx = lockdoc_get_ctx();
	/*
	 * One could use a more safe string function, e.g., strlcpy. 
	 * However, we want these *log* functions to be fast.
	 * We therefore skip all sanity checks, and all that stuff.
	 * To ensure any string buffer contains a valid string, we
	 * always write a NULL byte at its end.
	 */
	strncpy(la_buffer.type,datatype,LOG_CHAR_BUFFER_LEN);
	la_buffer.type[LOG_CHAR_BUFFER_LEN - 1] = '\0';
	__outb(PING_CHAR,IO_PORT_LOG);

	lockdoc_intr_restore(flags);
}

/*
 * The parameter irqsync should actually be an enum IRQ_SYNC. To avoid circular dependencies in include/linux/{irqflags,bottom_half}.h which contains
 * a declaration of this function, this parameter has to be an int.
 */
static inline void log_lock(int lock_op, const struct lock_object *ptr, const char *file, int line, int irq_sync) {
	register_t flags;
	struct lock_class *class;
	const struct lock_object *lock_object = (const struct lock_object*)ptr;
	flags = lockdoc_intr_disable();

	memset(&la_buffer,0,sizeof(la_buffer));

	la_buffer.action = LOCKDOC_LOCK_OP;
	la_buffer.lock_op = lock_op;
	la_buffer.ctx = lockdoc_get_ctx();
	/*
	 * ! ! ! IMPORTANT NOTICE ! ! !
	 * We strictly rely on the fact that an instance of struct lock_object
	 * is *always* embedded at the beginning of a struct {mtx, sx, rwlock}.
	 * Otherwise, we cannot use ptr as the pointer to the beginning of the lock. 
	 */
	la_buffer.ptr = (unsigned long)ptr;
	if (ptr == (struct lock_object*)PSEUDOLOCK_ADDR_HARDIRQ) {
		strncpy(la_buffer.type, PSEUDOLOCK_NAME_HARDIRQ,LOG_CHAR_BUFFER_LEN);
		strncpy(la_buffer.lock_member, PSEUDOLOCK_VAR_HARDIRQ,LOG_CHAR_BUFFER_LEN);
	} else {
		class = lock_classes[LO_CLASSINDEX(lock_object)];
		strncpy(la_buffer.type, class->lc_name,LOG_CHAR_BUFFER_LEN);
		la_buffer.flags = (lock_object->lo_flags & LO_RECURSABLE ? LOCK_FLAGS_RECURSIVE  : 0);
		strncpy(la_buffer.lock_member, "dummy",LOG_CHAR_BUFFER_LEN);
	}
	la_buffer.type[LOG_CHAR_BUFFER_LEN - 1] = '\0';
	la_buffer.lock_member[LOG_CHAR_BUFFER_LEN - 1] = '\0';
	strncpy(la_buffer.file,file,LOG_CHAR_BUFFER_LEN);
	la_buffer.file[LOG_CHAR_BUFFER_LEN - 1] = '\0';
	strncpy(la_buffer.function, "dummy",LOG_CHAR_BUFFER_LEN);
	la_buffer.function[LOG_CHAR_BUFFER_LEN - 1] = '\0';
	la_buffer.line = line;
	la_buffer.irq_sync = irq_sync;
	__outb(PING_CHAR,IO_PORT_LOG);

	lockdoc_intr_restore(flags);
}

void lockdoc_send_current_task_addr(void);
void lockdoc_send_irq_nest_offset(void);
void lockdoc_send_pid_offset(void);
void lockdoc_send_kernel_version(void);
#else
#define TRACE_IRQS_ON_CHECK(x)
#define TRACE_IRQS_ON(x)
#define TRACE_IRQS_OFF_CHECK(x)
#define TRACE_IRQS_OFF(x)
#define trace_irqs_off(x)
#define trace_irqs_on(x)
#define log_memory(w, x, y, z)
#define log_lock(a, b, c, d, e)
#define lockdoc_send_current_task_addr()
#define lockdoc_send_irq_nest_offset()
#define lockdoc_send_pid_offset()
#define lockdoc_send_kernel_version()
#endif // !LOCKDOC_RECORD

#endif // __LOCKDOC_H__
